extern crate sqlite;
extern crate clap;
extern crate naivebayes;
extern crate regex;
extern crate colored;

use sqlite::{Connection, Result, Statement};
use clap::{Arg, App, SubCommand};
use std::collections::HashMap;
use std::env;
use sqlite::Value;
use sqlite::State;
use naivebayes::NaiveBayes;
use regex::Regex;
use std::borrow::Cow;
use colored::*;

struct DuplicateNote {
    title: String,
    count: i64
}

pub struct SimpleNote {
    title: String,
    id: i64,
    identifier: String
}

pub struct SimpleNoteWithTags {
    title: String,
    id: i64,
    identifier: String,
    tags: String
}

pub struct Note {
    title: String,
    id: i64,
    identifier: String,
    tags: String,
    text: String
}

fn open_db() -> Connection {
    let home = env::home_dir().unwrap().to_str().unwrap().to_string();
    let connection = sqlite::open(home +
        "/Library/Group Containers/9K33E3U3T4.net.shinyfrog.bear/Application Data/database.sqlite").unwrap();
    return connection;
}

pub fn get_duplicates() -> Vec<SimpleNote> {
    let connection = open_db();
    let mut cursor = connection
    .prepare("select notes.Z_PK as id, notes.ZTITLE as title, notes.ZUNIQUEIDENTIFIER as identifier
                from ZSFNOTE as notes
              inner join
              (select dupe.ZTITLE as title, count(dupe.ZTITLE) as count 
                from ZSFNOTE as dupe group by dupe.ZTITLE having ( count > 1 )) d
              on notes.ZTITLE = d.title")
    .unwrap()
    .cursor();

    let mut rows: Vec<SimpleNote> = vec![];
    while let Some(row) = cursor.next().unwrap() {
        rows.push(SimpleNote {
            title: row[1].as_string().unwrap_or("").to_string(),
            id: row[0].as_integer().unwrap_or(0),
            identifier: row[2].as_string().unwrap_or("").to_string()
        });
    }

    return rows;
}

pub fn get_all_simple_notes() -> Vec<SimpleNoteWithTags> {
    let connection = open_db();
    let mut statement = connection
        .prepare("select n.Z_PK as id,
    n.ZTEXT as text,
    n.ZTITLE as title,
    group_concat(t.ZTITLE) as tags
    from ZSFNOTE as n left join Z_6TAGS as tn on n.Z_PK=tn.Z_6NOTES
    left join ZSFNOTETAG as t on tn.Z_13TAGS=t.Z_PK
    group by id;").unwrap();

    let mut rows: Vec<SimpleNoteWithTags> = vec![];

    while let State::Row = statement.next().unwrap() {
        rows.push(SimpleNoteWithTags {
            id: statement.read::<i64>(0).unwrap_or(0),
            title: statement.read::<String>(1).unwrap_or("".to_string()),
            identifier: statement.read::<String>(2).unwrap_or("".to_string()),
            tags: statement.read::<String>(3).unwrap_or("".to_string()),
        });
    }
    return rows;
}

pub fn get_head(number : i64) -> Vec<SimpleNoteWithTags> {
    let connection = open_db();
    let mut statement = connection
    .prepare("select n.Z_PK as id, n.ZTITLE as title, n.ZUNIQUEIDENTIFIER as identifier,
        group_concat(t.ZTITLE) as tags
        from ZSFNOTE as n left join Z_6TAGS as tn on n.Z_PK=tn.Z_6NOTES
        left join ZSFNOTETAG as t on tn.Z_13TAGS=t.Z_PK
        group by identifier
        order by id desc
        limit ?;")
    .unwrap();

    statement.bind(1, number).unwrap();

    let mut rows: Vec<SimpleNoteWithTags> = vec![];

    while let State::Row = statement.next().unwrap() {
        rows.push(SimpleNoteWithTags {
            id: statement.read::<i64>(0).unwrap_or(0),
            title: statement.read::<String>(1).unwrap_or("".to_string()),
            identifier: statement.read::<String>(2).unwrap_or("".to_string()),
            tags: statement.read::<String>(3).unwrap_or("".to_string()),
        });
    }
    return rows;
}

pub fn get_tail(number : i64) -> Vec<SimpleNoteWithTags> {
    let connection = open_db();
    let mut statement = connection
        .prepare("select n.Z_PK as id, n.ZTITLE as title, n.ZUNIQUEIDENTIFIER as identifier,
        group_concat(t.ZTITLE) as tags
        from ZSFNOTE as n left join Z_6TAGS as tn on n.Z_PK=tn.Z_6NOTES
        left join ZSFNOTETAG as t on tn.Z_13TAGS=t.Z_PK
        group by identifier
        order by id asc
        limit ?;")
        .unwrap();

    statement.bind(1, number).unwrap();

    let mut rows: Vec<SimpleNoteWithTags> = vec![];

    while let State::Row = statement.next().unwrap() {
        rows.push(SimpleNoteWithTags {
            id: statement.read::<i64>(0).unwrap_or(0),
            title: statement.read::<String>(1).unwrap_or("".to_string()),
            identifier: statement.read::<String>(2).unwrap_or("".to_string()),
            tags: statement.read::<String>(3).unwrap_or("".to_string()),
        });
    }
    return rows;
}

fn get_duplicates_action() {

    let duplicates: Vec<SimpleNote> = get_duplicates();
    println!("Found {} duplicates", duplicates.len());
    let mut counts: HashMap<String, Vec<String>> = HashMap::new();
    for d in duplicates {
        let title = d.title.clone();
        counts.entry(title).or_insert(vec![]).push(d.identifier);
    }
    let mut total = 0;
    for (k, v) in counts.iter() {
        total += v.len();
        println!("{} ({})", k, v.len());
        let mut counter = 0;
        for entry in v {
            counter +=1;
            if counter==v.len() {
                println!("└─bear://x-callback-url/open-note?id={}", entry);
            } else {
                println!("├─bear://x-callback-url/open-note?id={}", entry);
            }
            
        }
    }
    println!("Total: {}", total);
    
}

fn tag_suggest_action(title: String) -> (String, f64) {
    let notes = get_all_simple_notes();
    let mut nb = NaiveBayes::new();
    for note in notes {
        let title_tokens:Vec<String> = note.title.split(",").map(|s| s.to_string()).collect();
        let tag_tokens = note.tags.split(",");
        for tag in tag_tokens {
            if tag != "" {
                nb.train(&title_tokens, &tag.to_string())
            }
        }
    }
    let class_tokens:Vec<String> = title.split(",").map(|s| s.to_string()).collect();
    let classes = nb.classify(&class_tokens);
    let mut max_class= "".to_string();
    let mut max_val = 0.0;
    for (klass, prob) in classes {
        if prob > max_val {
            max_val = prob;
            max_class = klass;
        }
    }
    return (max_class.to_string(), max_val);
}

pub fn get_all_marked() -> Vec<Note> {
    let connection = open_db();
    let mut statement = connection
        .prepare("select n.Z_PK as id,
                                n.ZTITLE as title,
                                n.ZUNIQUEIDENTIFIER as identifier,
                                group_concat(t.ZTITLE) as tags,
                                n.ZTEXT as text
                                from ZSFNOTE as n left join Z_6TAGS as tn on n.Z_PK=tn.Z_6NOTES
                                left join ZSFNOTETAG as t on tn.Z_13TAGS=t.Z_PK
                                where text like \"%::%::%\"
                                group by id").unwrap();
    let mut rows: Vec<Note> = vec![];

    while let State::Row = statement.next().unwrap() {
        rows.push(Note {
            id: statement.read::<i64>(0).unwrap_or(0),
            title: statement.read::<String>(1).unwrap_or("".to_string()),
            identifier: statement.read::<String>(2).unwrap_or("".to_string()),
            tags: statement.read::<String>(3).unwrap_or("".to_string()),
            text: statement.read::<String>(4).unwrap_or("".to_string()),
        });
    }
    return rows;

}

fn marked_action() {
    let notes = get_all_marked();
    let cb = Regex::new(r"```[a-z]*\n[\s\S]*?\n```").unwrap();
    let r = Regex::new(r"::(.*)::").unwrap();

    for note in notes {
        let clean_text = cb.replace(&note.text, "");
        println!("⁂");
        println!("{}", note.title.blue());
        let matches = r.captures(&clean_text);

        match matches {
            Some(m) => {
                for c in m.iter().skip(1) {
                    println!("{}", c.unwrap().as_str().green());
                }
            },
            None => {}
        }
    }
}

fn main() {

    let matches = App::new("grizzly")
                        .version("0.0.4")
                        .author("Rui Vieira <ruidevieira@googlemail.com>")
                        .about("A Bear.app workflow enhancer")
                        .arg(Arg::with_name("duplicates")
                            .short("d")
                            .long("duplicate")
                            .help("Find duplicates by title")
                            .takes_value(false))
                        .arg(Arg::with_name("head")
                            .long("head")
                            .value_name("NUMBER")
                            .help("List the most recent entries")
                            .takes_value(true))
                        .arg(Arg::with_name("tail")
                            .long("tail")
                            .value_name("NUMBER")
                            .help("List the oldest entries")
                            .takes_value(true))
                        .arg(Arg::with_name("tagsuggest")
                            .short("ts")
                            .long("tag-suggest")
                            .value_name("TITLE")
                            .help("Suggest a tag based on your current data")
                            .takes_value(true))
                        .arg(Arg::with_name("marked")
                            .short("m")
                            .long("marked")
                            .help("Find marked passages in all notes")
                            .takes_value(false))


        .get_matches();
    
    if matches.is_present("duplicates") {
        get_duplicates_action();
    }

    if matches.is_present("head") {
        let config = matches.value_of("head").unwrap_or("10");
        let head = get_head(config.parse::<i64>().unwrap());
        for n in head {
            println!("identifier: {}\ntitle: {}\ntags: {}", n.identifier, n.title, n.tags)
        }
    }
    if matches.is_present("tail") {
        let config = matches.value_of("tail").unwrap_or("10");
        let head = get_tail(config.parse::<i64>().unwrap());
        for n in head {
            println!("identifier: {}\ntitle: {}\ntags: {}", n.identifier, n.title, n.tags)
        }
    }

    if matches.is_present("tagsuggest") {
        let config = matches.value_of("tagsuggest").unwrap_or("Test tag");

        let suggested = tag_suggest_action(config.to_string());
        println!("Suggested tags for '{}':\n{} ({}%)", config, suggested.0, suggested.1 * 100.0);
        
    }

    if matches.is_present("marked") {
        marked_action();
    }


}
