# grizzly

![](docs/bear.jpg)

Utility to enhance your [Bear.app](https://bear.app/) workflow.

> **warning**
>
> `grizzly` only performs read operations on Bear's main database.
> However, it is always possible that the database is corrupted or left in an
> inconsistent state.
>
> Please backup your Bear database before using this if you don't want to
> risk loosing your data. 



## features

* `-d`, Show duplicate titles and counts
* `-m`, List all marked passages in all notes
* `--tail n`, `--head n`, Get `n` oldest or latest entries (by id)
* `--tag-suggest TITLE`, Suggest a tag for `TITLE` based on your notes